//
//  _0190820_KennySanchez_NYCSchoolsTests.swift
//  20190820-KennySanchez-NYCSchoolsTests
//
//  Created by Kenny Sanchez on 8/20/19.
//  Copyright © 2019 Kenny Sanchez. All rights reserved.
//

import XCTest
@testable import _0190820_KennySanchez_NYCSchools
import Testing
import KSCore


class _0190820_KennySanchez_NYCSchoolsTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    var nycSchoolsJsonData: Data {
        return FixtureGenerator().jsonData(fixture: "nycSchools")
    }

    var nycSchoolSATScores: Data {
        return FixtureGenerator().jsonData(fixture: "nycSchoolSATScores")
    }

    func testNYCSchoolsLoad() throws {
        
        let result = try JSONDecoder().decode([School].self, from: nycSchoolsJsonData)

        XCTAssertNotNil(result)
        XCTAssertEqual(result.count, 440)
        XCTAssertEqual(result[0].dbn, "02M260")
        XCTAssertEqual(result[0].schoolName, "Clinton School Writers & Artists, M.S. 260")
        XCTAssertEqual(result[0].location, "10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)")

        XCTAssertEqual(result[8].dbn, "09X505")
        XCTAssertNil(result[8].schoolEmail)
        XCTAssertEqual(result[8].schoolName, "Bronx School for Law, Government and Justice")
        XCTAssertEqual(result[8].location, "244 East 163rd Street, Bronx NY 10451 (40.827461, -73.919024)")
        
        XCTAssertEqual(result[result.count - 1].dbn, "14K477")
        XCTAssertEqual(result[result.count - 1].schoolName, "School for Legal Studies")
        XCTAssertEqual(result[result.count - 1].location, "850 Grand Street, Brooklyn NY 11211 (40.711134, -73.938921)")
        
    }
    
    func testNYCSchoolsLoadViaURL() throws {
        
        let nycSchoolsLoadExpectation = self.expectation(description: "NYC Schools load")
        
        let nycSchoolsAPIService: SchoolAPIService = APIService()
        nycSchoolsAPIService.fetchSchools(userIdentifier: 1234567) { responseObject, error in
            
            guard let responseNYCSchools = responseObject, let nycSchools = responseNYCSchools["schools"] else {
                return
            }

            let result = nycSchools as! [School]
            XCTAssertNotNil(result)
            XCTAssertEqual(result.count, 440)

            nycSchoolsLoadExpectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 1) { (error) in
            
        }

    }

    func testNYCSchoolSATScoresLoad() throws {
        
        let result = try JSONDecoder().decode([SATScore].self, from: nycSchoolSATScores)
        
        XCTAssertNotNil(result)
        XCTAssertEqual(result.count, 478)
        
        XCTAssertEqual(result[0].dbn, "01M292")
        XCTAssertEqual(result[0].schoolName, "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES")
        XCTAssertEqual(result[0].numOfSatTestTakers, "29")
        XCTAssertEqual(result[0].satCriticalReadingAvgScore, "355")
        XCTAssertEqual(result[0].satMathAvgScore, "404")
        XCTAssertEqual(result[0].satWritingAvgScore, "363")

        
        XCTAssertEqual(result[result.count - 1].dbn, "79X490")
        XCTAssertEqual(result[result.count - 1].schoolName, "PHOENIX ACADEMY")
        XCTAssertEqual(result[result.count - 1].numOfSatTestTakers, "9")
        XCTAssertEqual(result[result.count - 1].satCriticalReadingAvgScore, "367")
        XCTAssertEqual(result[result.count - 1].satMathAvgScore, "370")
        XCTAssertEqual(result[result.count - 1].satWritingAvgScore, "360")

    }

    
}
