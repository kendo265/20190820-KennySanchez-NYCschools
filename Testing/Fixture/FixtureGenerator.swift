//
//  FixtureGenerator.swift
//  Testing
//
//  Created by Kenny Sanchez on 8/20/19.
//  Copyright © 2019 Kenny Sanchez. All rights reserved.
//

import Foundation

public final class FixtureGenerator {
    
    let bundle: Bundle
    let identifier: String?
    
    public init(identifier: String? = nil) {
        self.bundle = Bundle(for: type(of: self))
        self.identifier = identifier
    }
    
}

extension FixtureGenerator {
    
    public func jsonData(fixture: String) -> Data {
        
        guard let fixtureFile = bundle.path(forResource: fixture, ofType: ".json") else {
            return Data()
        }
        
        let url  = URL(fileURLWithPath: fixtureFile)
        
        do {
            let jsonData = try Data(contentsOf: url)
            return jsonData
        } catch {
            fatalError("Error while parsing fixture: \(error)")
        }
        
    }
    
}
