//
//  SchoolAPIService.swift
//  KSCore
//
//  Created by Kenny Sanchez on 8/20/19.
//  Copyright © 2019 Kenny Sanchez. All rights reserved.
//

import Foundation

public protocol SchoolAPIService: class {
    
    func fetchSchools(userIdentifier: Int64, complete: @escaping CompleteResponseOrError)
    
}
