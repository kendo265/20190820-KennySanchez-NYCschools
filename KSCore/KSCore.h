//
//  KSCore.h
//  KSCore
//
//  Created by Kenny Sanchez on 8/20/19.
//  Copyright © 2019 Kenny Sanchez. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for KSCore.
FOUNDATION_EXPORT double KSCoreVersionNumber;

//! Project version string for KSCore.
FOUNDATION_EXPORT const unsigned char KSCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <KSCore/PublicHeader.h>


