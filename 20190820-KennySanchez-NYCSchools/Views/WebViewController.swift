//
//  WebViewController.swift
//  20190820-KennySanchez-NYCSchools
//
//  Created by Kenny Sanchez on 8/21/19.
//  Copyright © 2019 Kenny Sanchez. All rights reserved.
//

import WebKit

class WebViewController: UIViewController {

    @IBOutlet weak var schoolWebView: WKWebView!
    @IBOutlet weak var DoneButton: UIButton!
    
    var school: School?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //TODO: If had more time why url not loading e.g. schools.nyc.gov/SchoolPortals/17/K751
        if let schoolURL = school?.website {
            let myURL = URL(string: schoolURL)
            let myRequest = URLRequest(url: myURL!)
            schoolWebView.load(myRequest)
        }
        
    }
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
