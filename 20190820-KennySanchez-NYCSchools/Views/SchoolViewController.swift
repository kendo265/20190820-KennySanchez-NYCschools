//
//  SchoolViewController.swift
//  20190820-KennySanchez-NYCSchools
//
//  Created by Kenny Sanchez on 8/20/19.
//  Copyright © 2019 Kenny Sanchez. All rights reserved.
//

import UIKit

class SchoolViewController: UIViewController {
    
    private var schools         = [School]()
    private var filteredSchools = [School]()
    private let schoolPresenter = SchoolPresenter()


    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var noSchoolsView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate   = self
        schoolPresenter.attachView(view: self)
        schoolPresenter.fetchSchools()
        
        filteredSchools = schools
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
    }

    
}

extension SchoolViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        
        if searchController.searchBar.text! == "" {
            filteredSchools = schools
        } else {
            filteredSchools = schools.filter { ($0.schoolName.lowercased().contains(searchController.searchBar.text!.lowercased())) }
        }
        
        self.tableView.reloadData()
    }
    
    
}

extension SchoolViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredSchools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell                   = UITableViewCell(style: .subtitle, reuseIdentifier: "SchoolCell")
        cell.textLabel?.text = self.filteredSchools[indexPath.row].schoolName
        cell.detailTextLabel?.text = self.filteredSchools[indexPath.row].website
        return cell
    }
    
}

extension SchoolViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        schoolPresenter.didSelectSchool(school: filteredSchools[indexPath.row] )
    }
    
}

extension SchoolViewController: SchoolView {
    
    func displayDetailSchoolSATScores(school: School) {
        let schoolScoresViewController = UIStoryboard.schoolSATScoresViewController()
        schoolScoresViewController.school = school

        navigationController?.pushViewController(schoolScoresViewController,
                                                 animated: true)
    }
    
    func startLoading() {
        activityIndicatorView?.startAnimating()
    }
    
    func finishLoading() {
        activityIndicatorView?.stopAnimating()
    }
    
    func setSchools(schools: [School]) {
        self.schools            = schools
        self.filteredSchools    = schools
        tableView?.isHidden     = false
        noSchoolsView?.isHidden = true
        tableView?.reloadData()
    }
    
    func setEmptySchools() {
        tableView?.isHidden     = true
        noSchoolsView?.isHidden = false
    }
    
}

