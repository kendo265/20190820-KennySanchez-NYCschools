//
//  SchoolView.swift
//  20190820-KennySanchez-NYCSchools
//
//  Created by Kenny Sanchez on 8/20/19.
//  Copyright © 2019 Kenny Sanchez. All rights reserved.
//

import Foundation

protocol SchoolView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func setSchools(schools: [School])
    func setEmptySchools()
    func displayDetailSchoolSATScores(school: School)
}
