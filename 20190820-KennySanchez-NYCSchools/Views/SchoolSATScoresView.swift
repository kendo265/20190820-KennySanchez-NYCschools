//
//  SchoolSATScoresView.swift
//  20190820-KennySanchez-NYCSchools
//
//  Created by Kenny Sanchez on 8/20/19.
//  Copyright © 2019 Kenny Sanchez. All rights reserved.
//

import Foundation

protocol SchoolSATScoresView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func setSchoolScores(schoolSATScores:  SATScore)
    func setEmptySchoolScores()
}
