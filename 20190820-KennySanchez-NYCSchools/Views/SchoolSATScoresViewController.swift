//
//  SchoolSATScoresViewController.swift
//  20190820-KennySanchez-NYCSchools
//
//  Created by Kenny Sanchez on 8/20/19.
//  Copyright © 2019 Kenny Sanchez. All rights reserved.
//

import Foundation
import UIKit

class SchoolSATScoresViewController: UIViewController {
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    private let schoolSATScoresPresenter = SchoolSATScoresPresenter()
    var school: School?
    private var schoolSATScores: SATScore?
    
//    @IBOutlet weak var noSATScoresView: UIView!
//    @IBOutlet weak var noSATScoresSchoolNameLabel: UILabel!
    @IBOutlet weak var satScoresView: UIView!
    
    //score labels
    @IBOutlet weak var schoolNameHeaderLabel: UILabel!
    @IBOutlet weak var criticalReadingAvgScoreLabel: UILabel!
    @IBOutlet weak var mathAvgScoreLabel: UILabel!
    @IBOutlet weak var writingAvgScore: UILabel!
    @IBOutlet weak var numberOfSATTakers: UILabel!
    @IBOutlet weak var visitSchoolWebsiteButton: UIButton!
    
    @IBOutlet weak var satScoresNotFoundLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.visitSchoolWebsiteButton.isEnabled = false //TODO:  If had more time enable after figure out why website for schools not displaying.  Was able to display apple.com and cnn.com 
        self.navigationController?.title = school?.schoolName
        schoolNameHeaderLabel.text       = school?.schoolName
        self.view.backgroundColor        = .white
        schoolSATScoresPresenter.attachView(view: self)
        
        guard let dbn = school?.dbn else { return }
        schoolSATScoresPresenter.fetchSchoolSATScores(dbn: dbn)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? WebViewController {
            destinationVC.school = self.school
        }

    }
    
}

extension SchoolSATScoresViewController: SchoolSATScoresView {
    func startLoading() {
        activityIndicatorView?.startAnimating()
    }
    
    func finishLoading() {
        activityIndicatorView?.stopAnimating()
    }
    
    func setSchoolScores(schoolSATScores: SATScore) {
        self.schoolSATScores = schoolSATScores
        
        //Set Accessibility attributes
        schoolNameHeaderLabel.text        = self.schoolSATScores?.schoolName
        
        let satCriticalReadingAvgScore = self.schoolSATScores?.satCriticalReadingAvgScore ?? "N\\A"
        criticalReadingAvgScoreLabel.text = "SAT Critical Reading Average Score: \(satCriticalReadingAvgScore)"
        
        let satMathAvgScore = self.schoolSATScores?.satMathAvgScore ?? "N\\A"
        mathAvgScoreLabel.text            = "SAT Math Average Score: \(satMathAvgScore)"
        
        let satWritingAvgScore = self.schoolSATScores?.satWritingAvgScore ?? "N\\A"
        writingAvgScore.text              = "SAT Writing Average Score: \(satWritingAvgScore)"

        let numOfSatTestTakers = self.schoolSATScores?.numOfSatTestTakers ?? "N\\A"
        numberOfSATTakers.text            = "Number of SAT Test Takers: \(numOfSatTestTakers)"
        
        satScoresNotFoundLabel?.isHidden     = true
        satScoresView?.isHidden              = false
        schoolNameHeaderLabel?.isHidden      = false
        criticalReadingAvgScoreLabel?.isHidden = false
        mathAvgScoreLabel?.isHidden          = false
        writingAvgScore?.isHidden            = false
        numberOfSATTakers?.isHidden          = false
        visitSchoolWebsiteButton?.isHidden   = false

    }
    
    func setEmptySchoolScores() {
        satScoresNotFoundLabel?.isHidden     = false
        schoolNameHeaderLabel?.isHidden      = false
        satScoresView?.isHidden              = true
        criticalReadingAvgScoreLabel?.isHidden = true
        mathAvgScoreLabel?.isHidden          = true
        writingAvgScore?.isHidden            = true
        numberOfSATTakers?.isHidden          = true
        visitSchoolWebsiteButton?.isHidden   = true

    }
}

extension UIStoryboard {
    
    static func schoolSATScoresViewController() -> SchoolSATScoresViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SchoolSATScoresViewController") as! SchoolSATScoresViewController
    }
}
