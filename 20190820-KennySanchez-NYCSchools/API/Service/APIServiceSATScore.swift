//
//  APIServiceSATScore.swift
//  20190820-KennySanchez-NYCSchools
//
//  Created by Kenny Sanchez on 8/20/19.
//  Copyright © 2019 Kenny Sanchez. All rights reserved.
//

import KSCore

extension APIService: SATScoreAPIService {
    
    public func fetchSATScoresFor(userIdentifier: Int64, dbn: String, complete: @escaping CompleteResponseOrError) {

        //TODO:  Add dbn to parameters array
        get(Constants.nycSchoolSATScoresURL+"?dbn=\(dbn)", parameters: nil) { (data, error) in
            
            do {
                guard let data = data else {
                    return
                }
                
                let result = try JSONDecoder().decode(Array<SATScore>.self, from: data["data"] as! Data)
                complete(["satScores": result], nil)
            } catch {
                complete(nil, error)
            }
            
        }
        
    }
    
}
