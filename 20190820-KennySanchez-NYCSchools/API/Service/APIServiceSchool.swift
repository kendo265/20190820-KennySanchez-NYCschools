//
//  APIServiceSchool.swift
//  20190820-KennySanchez-NYCSchools
//
//  Created by Kenny Sanchez on 8/20/19.
//  Copyright © 2019 Kenny Sanchez. All rights reserved.
//

import KSCore

extension APIService: SchoolAPIService {
    
    public func fetchSchools(userIdentifier: Int64, complete: @escaping CompleteResponseOrError) {
        
        get(Constants.nycSchoolsURL, parameters: nil) { (data, error) in
            
            do {
                guard let data = data else {
                    return
                }
                
                let result = try JSONDecoder().decode(Array<School>.self, from: data["data"] as! Data)
                complete(["schools": result], nil)
            } catch {
                complete(nil, error)
            }
            
        }
        
    }
    
}
