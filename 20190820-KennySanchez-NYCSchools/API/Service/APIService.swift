//
//  APIService.swift
//  20190820-KennySanchez-NYCSchools
//
//  Created by Kenny Sanchez on 8/20/19.
//  Copyright © 2019 Kenny Sanchez. All rights reserved.
//

import Foundation
import KSCore

public final class APIService: APIServiceType {
    
    public func get(_ url: String, parameters: Any?, complete: @escaping CompleteResponseOrError) {
        
        guard let urlx = URL(string: url) else {
            return
        }
        
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: urlx)
        request.httpMethod = RequestVerb.get.rawValue
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let task = session.dataTask(with: request as URLRequest) {(data, response, error) in
            
            if let error = error {
                complete(nil, error)
            }
            
            if let data = data {
                complete(["data": data], nil)
            }
        }
        
        task.resume()
        
    }
    
}

