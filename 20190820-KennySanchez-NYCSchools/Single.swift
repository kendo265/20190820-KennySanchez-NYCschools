//
//  Single.swift
//  20190820-KennySanchez-NYCSchools
//
//  Created by Kenny Sanchez on 8/20/19.
//  Copyright © 2019 Kenny Sanchez. All rights reserved.
//

import Foundation
import KSCore

public class Single {
    
    private static let shared = Single()
    private let apiService: APIService
    
    init() {
        apiService               = APIService()
    }
    
    public static var apiService: APIService {
        return shared.apiService
    }
    
}
