//
//  Constants.swift
//  20190820-KennySanchez-NYCSchools
//
//  Created by Kenny Sanchez on 8/20/19.
//  Copyright © 2019 Kenny Sanchez. All rights reserved.
//

import Foundation

public struct Constants {
    
    static let nycSchoolsURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$select=dbn,school_name,boro,overview_paragraph,location,phone_number,fax_number,school_email,website,subway,bus,total_students,primary_address_line_1,city,zip,state_code&$order=school_name%20ASC"
    static let nycSchoolSATScoresURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
}
