//
//  SchoolPresenter.swift
//  20190820-KennySanchez-NYCSchools
//
//  Created by Kenny Sanchez on 8/20/19.
//  Copyright © 2019 Kenny Sanchez. All rights reserved.
//

import Foundation
import KSCore

final class SchoolPresenter {
    
    weak private var schoolView: SchoolView?
    
    func attachView(view: SchoolView) {
        self.schoolView = view
    }
    
    func detachView() {
        self.schoolView = nil
    }
    
    func fetchSchools() {
        self.schoolView?.startLoading()

        Single.apiService.fetchSchools(userIdentifier: 1234567) { [weak self] responseObject, error in
            
            DispatchQueue.main.async {
                self?.schoolView?.finishLoading()
                
                if let response = responseObject, let schools = response["schools"], let theSchools = schools as? [School] {
                    
                    if theSchools.count > 0 {
                        self?.schoolView?.setSchools(schools: theSchools)
                    } else {
                        self?.schoolView?.setEmptySchools()
                    }
                    
                } else {
                    self?.schoolView?.setEmptySchools()
                }
                
            }
            
        }

    }
    
    func didSelectSchool(school: School) {
        //Load school sat scores detail view
        self.schoolView?.displayDetailSchoolSATScores(school: school)
    }
    
}
