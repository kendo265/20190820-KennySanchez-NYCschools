//
//  SchoolSATScoresPresenter.swift
//  20190820-KennySanchez-NYCSchools
//
//  Created by Kenny Sanchez on 8/20/19.
//  Copyright © 2019 Kenny Sanchez. All rights reserved.
//

import Foundation

final class SchoolSATScoresPresenter {
    
    weak private var schoolSATScoresView: SchoolSATScoresView?
    
    func attachView(view: SchoolSATScoresView) {
        self.schoolSATScoresView = view
    }
    
    func detachView() {
        self.schoolSATScoresView = nil
    }
    
    func fetchSchoolSATScores(dbn: String) {
        self.schoolSATScoresView?.startLoading()
        
        Single.apiService.fetchSATScoresFor(userIdentifier: 123456, dbn: dbn) { [weak self] responseObject, error in
            
            DispatchQueue.main.async {
                self?.schoolSATScoresView?.finishLoading()
                
                if let response = responseObject, let satScores = response["satScores"], let theSATScores = satScores as? [SATScore] {
                    
                    if theSATScores.count > 0 {
                        self?.schoolSATScoresView?.setSchoolScores(schoolSATScores: theSATScores[0])
                    } else {
                        self?.schoolSATScoresView?.setEmptySchoolScores()
                    }
                    
                } else {
                    self?.schoolSATScoresView?.setEmptySchoolScores()
                }
                
            }
            
        }
        
    }
    
}
