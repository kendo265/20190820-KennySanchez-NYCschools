//
//  School.swift
//  20190820-KennySanchez-NYCSchools
//
//  Created by Kenny Sanchez on 8/20/19.
//  Copyright © 2019 Kenny Sanchez. All rights reserved.
//

import Foundation


//$select=dbn, school_name, boro, overview_paragraph, location, phone_number, fax_number, school_email, website, subway, bus,  total_students, primary_address_line_1, city, zip, state_code

struct School: Codable {
    var dbn: String
    var schoolName: String
    var boro: String?
    var overviewParagraph: String?
    var location: String?
    var phoneNumber: String?
    var faxNumber: String?
    var schoolEmail: String?
    var website: String?
    var subway: String?
    var bus: String?
    var totalStudents: String?
    var primaryAddressLine1: String?
    var city: String?
    var zip: String?
    var stateCode: String?
    
    private enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case boro
        case overviewParagraph = "overview_paragraph"
        case location
        case phoneNumber = "phone_number"
        case faxNumber   = "fax_number"
        case schoolEmail = "school_email"
        case website
        case subway
        case bus
        case totalStudents = "total_students"
        case primaryAddressLine1 = "primary_address_line_1"
        case city
        case zip
        case stateCode = "state_code"
    }
    
}
